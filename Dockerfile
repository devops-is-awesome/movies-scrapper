FROM node:alpine
WORKDIR /usr/src/app
ADD server.js .
ADD package.json .
ADD views views
RUN npm install
ENV PORT=80
EXPOSE 80
CMD ["node", "server.js"]
