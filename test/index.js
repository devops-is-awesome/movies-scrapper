var chai = require('chai'),
    chaiHttp = require('chai-http'),
    app = require('../server')

chai.use(chaiHttp)
chai.should()

describe("Movies Search API", () => {
    describe("GET /api/movies/:name", () => {
        it("should return a list of movies", (done) => {
            chai.request(app).get('/api/movies/Twilight').end((err, res) => {
                res.body.Search.should.be.a('array')
                done()
            })
        })

        it("should return an error", (done) => {
            chai.request(app).get('/api/movies/anything-not-existing').end((err, res) => {
                res.body.Error.should.be.equal('Movie not found!')
                done()
            })
        })
    })
})