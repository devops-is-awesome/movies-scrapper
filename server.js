var express = require('express'),
    path = require('path'),
    axios = require('axios'),
    app = express()

app.get('/api/movies/:name', async (req, res) => {
    var result = await axios.default.get('https://www.omdbapi.com/?plot=full&apikey=PlzBanM3&s=' + req.params.name)
    res.send(result.data)
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'))
})

app.listen(80)

module.exports = app